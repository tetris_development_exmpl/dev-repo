#!/usr/bin/env sh

set -eu

if [ ! -d "project" ]; then
  git clone "ssh://git@gitlab.com:tetris_development_exmpl/project.git" "project"
fi

echo "OK"
sleep 3
