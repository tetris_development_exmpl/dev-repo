#!/bin/bash

set -eu

dbName=${DB_NAME}
dbSchema="public"
dbOwner=${DB_USER}
dbPassword=${DB_PASS}

export PGUSER=postgres
export PGPASSWORD=postgres


function isDatabaseExists() {
  local database=$1

  local exists
  exists=$(
    psql -qtAX -c "SELECT EXISTS (SELECT FROM pg_database WHERE datname='$database')"
  )
  if [[ "$exists" == "t" ]]; then
    echo 1
  else
    echo 0
  fi
}

function createDb() {
  local database=$1
  local owner=$2

  PGOPTIONS='--client-min-messages=warning' psql -q -c \
    "DROP DATABASE IF EXISTS \"$database\";"
  psql -q -c \
    "CREATE DATABASE \"$database\" WITH OWNER = \"$owner\";"
}


function createSchema() {
  local database=$1
  local schema=$2
  local owner=$3

  forUser="postgres"


  PGOPTIONS='--client-min-messages=warning' psql -q -c \
    "CREATE SCHEMA IF NOT EXISTS \"$schema\";" "$database"
  psql -q -c \
    "ALTER SCHEMA \"$schema\" OWNER TO \"$owner\";" "$database"

  psql -q -c \
    "ALTER DEFAULT PRIVILEGES FOR ROLE $forUser IN SCHEMA \"$schema\" \
    GRANT ALL ON TABLES TO \"$owner\";" "$database"
  psql -q -c \
    "ALTER DEFAULT PRIVILEGES FOR ROLE $forUser IN SCHEMA \"$schema\" \
    GRANT ALL ON SEQUENCES TO \"$owner\";" "$database"
  psql -q -c \
    "ALTER DEFAULT PRIVILEGES FOR ROLE $forUser IN SCHEMA \"$schema\" \
    GRANT ALL ON FUNCTIONS TO \"$owner\";" "$database"
  psql -q -c \
    "ALTER DEFAULT PRIVILEGES FOR ROLE $forUser IN SCHEMA \"$schema\" \
    GRANT ALL ON TYPES TO \"$owner\";" "$database"
}


function grantReadPrivileges() {
  local database=$1
  local schema=$2
  local user=$3

  forUser="postgres"


  psql -q -c \
    "ALTER DEFAULT PRIVILEGES FOR ROLE $forUser IN SCHEMA \"$schema\" GRANT SELECT ON TABLES TO \"$user\";" "$database"
  psql -q -c \
    "ALTER DEFAULT PRIVILEGES FOR ROLE $forUser IN SCHEMA \"$schema\" GRANT SELECT, USAGE ON SEQUENCES TO \"$user\";" "$database"
  psql -q -c \
    "ALTER DEFAULT PRIVILEGES FOR ROLE $forUser IN SCHEMA \"$schema\" GRANT USAGE ON TYPES TO \"$user\";" "$database"
}

function createUser() {
  local username=$1
  local password=$2

  if [ ! "$( psql -tAc "SELECT 1 FROM pg_user WHERE usename = '$username'" )" = "1" ]; then
    psql -q -c "CREATE USER \"$username\" WITH PASSWORD '$password';"
  fi
  psql -q -c "ALTER ROLE \"$username\" NOSUPERUSER;"
}

if [ "$(isDatabaseExists "$dbName")" = "0" ]; then
  echo "Creating of the \"$dbName\" database is initiated."

  echo "Creating users"
  createUser "$dbOwner" "$dbOwner"

  echo "Creating database"
  createDb "${dbName}" "$dbOwner"
  createSchema "${dbName}" "$dbSchema" "$dbOwner"
  grantReadPrivileges "${dbName}" "$dbSchema" "${dbOwner}"

  echo "Done"
else
  echo "Database \"$dbName\" is already exists."
fi