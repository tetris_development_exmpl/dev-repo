FROM php:8.1-fpm

RUN apt-get update && apt-get -y install software-properties-common curl git iproute2 openssl unzip wget tzdata nano htop apt-utils libpq-dev gnupg2
RUN apt-get install -y libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql

RUN yes | pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini

COPY conf /conf

RUN mkdir -p /run/php && \
    mkdir -p /var/lib/php/sessions && \
    chmod 777 /var/lib/php/sessions && \
    chown root:www-data /var/lib/php/sessions

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ENV XDEBUG_SESSION 1
ENV PHP_IDE_CONFIG "serverName=project"

RUN mkdir /app
RUN mkdir -m 777 /app/xdebug && chgrp www-data /app/xdebug

WORKDIR /app

EXPOSE 9000

COPY docker-entrypoint.sh /
RUN chmod u+x /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["php-fpm"]
