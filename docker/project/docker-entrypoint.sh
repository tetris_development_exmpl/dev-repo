#!/bin/bash

set -eu

tz=${TZ:-UTC}
xdebugHost=$(/sbin/ip route | awk '/default / { print $3; }')
xdebugPort=${XDEBUG_PORT:-9003}

printf "date.timezone = %s\n" "$tz" \
  >${PHP_INI_DIR}/conf.d/30-timezone.ini
printf "xdebug.client_host = %s\nxdebug.client_port = %s\n" "$xdebugHost" "$xdebugPort" \
  >${PHP_INI_DIR}/conf.d/30-xdebug.ini
#printf "date.timezone = %s\n" "$tz" \
#  >/etc/php/8.1/fpm/conf.d/30-timezone.ini
#printf "xdebug.client_host = %s\nxdebug.client_port = %s\n" "$xdebugHost" "$xdebugPort" \
#  >/etc/php/8.1/fpm/conf.d/30-xdebug.ini
mv /conf/php.ini ${PHP_INI_DIR}/php.ini
mv /conf/php-fpm.conf ${PHP_INI_DIR}/php-fpm.conf
chmod 777 /app/xdebug
chgrp www-data /app/xdebug


exec "$@"
