Данный репозиторий создан для разворачивания дев инфраструктуры

## Подготовка
* скопируйте .env.example в .env
* скопируйте data/config/main-local.php.exml  в data/config/main-local.php
* скопируйте data/config/params-local.php.exml  в data/config/params-local.php
* если сабмодули не подтянулись - запустите clone.sh\clone.bat
* добавьте в /etc/hosts mytestsite.com и files.mytestsite.com на локалхост
* поднимите контейнеры командой docker-compose up -d --build
* проект запущен